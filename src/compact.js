// TODO:
//
// Implement a function called `compact`. This function creates an array with
// all falsey values removed. The values `false`, `null`, `0`, `""`, `undefined`,
// and `NaN` are falsey.
//
// You can refer to unit test for more details.
//
// <--start-
function compact (array) {
  const falsey = [false, null, 0, '', undefined, NaN];

  if (array == null || array === undefined) {
    return array;
  } else {
    const newArray = array.filter(element => falsey.includes(element) === false);
    return newArray;
  }
}
// --end-->

export default compact;
