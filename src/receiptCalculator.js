class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (items) {
    let totalPrice = 0;
    const idArray = [];
    for (let i = 0; i < this.products.length; i++) {
      idArray.push(this.products[i].id);
    }

    for (let i = 0; i < items.length; i++) {
      if (idArray.includes(items[i]) === false) {
        throw Error('Product not found.');
      } else {
        for (let j = 0; j < idArray.length; j++) {
          if (items[i] === idArray[j]) {
            totalPrice += this.products[j].price;
          }
        }
      }
    }
    return totalPrice;
  }
  // --end->
}

export default RecepitCalculator;
